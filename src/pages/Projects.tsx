import React from "react";
import { useEffect } from "react";
import { getProfileData } from "../services/api";
import { useState } from "react";
import RepositoriesByUser from "../components/RepositoriesByUser";
import GithubProfile from "../components/GitHubProfile";

const Projects: React.FC = () => {
  const [userInfo, setUserInfo]: {} = useState();

  useEffect(() => {
    const loadUserInfos: any = async () => {
      const info: {} = await getProfileData("shan15dev");
      setUserInfo(info);
    };
    loadUserInfos();
  });

  return (
    <div className="text-white text-center justify-center">
      <GithubProfile name="shan15dev" />
       <RepositoriesByUser name="Shan15Dev" />
    </div>
  );
};

export default Projects;
