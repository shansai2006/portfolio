import Header from "../components/Header";
import LinkTiles from "../components/LinkTiles";

export default function Home() {
  return (
    <div>
      <Header title="Home" />
      <div className="text-white text-center justify-center mt-5 grid gap-y-5 grid-cols-12 md:grid-cols-5">
        <div className="col-start-4 col-span-6 row-end-1 md:col-start-3">
          <LinkTiles path="/about" textTitle={"About Me"} />
        </div>
        <div className="col-start-4 col-span-6 row-end-2 md:col-start-3">
          <LinkTiles path="/projects" textTitle={"Projects"} />
        </div>
      </div>
    </div>
  );
}
