const URL: string = "https://api.github.com";

export async function getProfileData(name: string) {
  const response: {} = await fetch(`${URL}/users/${name}`);

  if (!response.ok) {
    return Promise.reject(response);
  }

  const data: {} = await response.json();

  return data;
}

export async function getUserRepositories(name: string) {
  const response: {} = await fetch(
    `${URL}/users/${name}/repos?sort=updated&direction=desc`
  );

  if (!response.ok) return Promise.reject(response);

  const data: {} = await response.json();

  return data;
}
