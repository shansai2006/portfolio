import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { getProfileData } from '../services/api';

const GithubProfile : React.FC = (props: {name : string}) => {
    const [userInfo, setUserInfo]: {} = useState();

    useEffect(() => {
        const loadUserInfos: any = async () => {
          const info: {} = await getProfileData(props.name);
          setUserInfo(info);
        };
        loadUserInfos();
      });

    return ( 
        <div>
            {userInfo != null && 
            <div>
                <a href={userInfo.html_url}>
                    <img src={userInfo.avatar_url} alt={`Profile logo of ${props.name}`} />
                </a>
            </div>
            }
        </div>
    )
}

export default GithubProfile