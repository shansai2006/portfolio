import React from 'react';

function Header({title}) {
    return (
        <header className="bg-white shadow dark:bg-slate-800">
            <div className="max-w-7xl mx-auto py-6 sm:px-6 md:px-8">
                <h1 className="text-3xl font-bold text-gray-900 dark:text-slate-100 text-center">{title}</h1>
            </div>
        </header>
    );
}

export default Header;
